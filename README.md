# eshop项目接口

#### 介绍
电商购物网站，接口方

#### 软件架构
软件架构说明


#### 安装教程

<!-- add docs here for user -->

see [egg docs][egg] for more detail.

##### Development

```bash
$ npm i
$ npm run dev
$ open http://localhost:7001/
```

##### Deploy

```bash
$ npm start
$ npm stop
```

##### npm scripts

- Use `npm run lint` to check code style.
- Use `npm test` to run unit test.
- Use `npm run autod` to auto detect dependencies upgrade, see [autod](https://www.npmjs.com/package/autod) for more detail.


[egg]: https://eggjs.org