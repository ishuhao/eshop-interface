/* eslint valid-jsdoc: "off" */

'use strict';

/**
 * @param {Egg.EggAppInfo} appInfo app info
 */
module.exports = appInfo => {
  /**
   * built-in config
   * @type {Egg.EggAppConfig}
   **/
  const config = exports = {};

  // use for cookie sign key, should change to your own and keep security
  config.keys = appInfo.name + '_1564400622590_4493';

  // 配置数据库
  config.sequelize = {
    dialect: 'mysql',
    database: 'eshop',
    host: 'localhost',
    port: '3306',
    username: 'root',
    password: 'tiger',
  };

  // 关闭csrf
  config.security = {
    csrf: {
      enable: false,
      ignoreJSON: true,
    },
    // 白名单
    domainWhiteList: [ '*' ],
  };

  // add your middleware config here
  config.middleware = [];

  // add your user config here
  const userConfig = {
    // myAppName: 'egg',
    filePath: 'E:/temp',
  };

  // 文件上传相关配置
  config.multipart = {
    fileSize: '50mb',
    mode: 'stream',
    fileExtensions: [ '.xls', '.txt', '.doc', '.docx', '.xlsx', '.png', '.jpg', '.jpeg', '.webp', '.ppt', '.pptx', '.zip', '.rar', '.jar' ],
  };

  return {
    ...config,
    ...userConfig,
  };
};
