'use strict';

module.exports = {
  SUCCESS: 0,
  UPDATE_FAIL: 1000,
  CREATE_FAIL: 1001,
  DELETE_FAIL: 1002,
  QUERY_FAIL: 1003,
  // 缺少参数
  MISS_PARAM: 1004,
  // 参数错误
  ERROR_PARAM: 1005,
  // 上传文件失败
  UPLOAD_FILE_ERROR: 1006,
  // 文件扩展名不合法
  EXT_NOT_RIGHT: 1007,
  // 下载文件失败
  DOWNLOAD_FILE: 1008,
};
