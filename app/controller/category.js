'use strict';

const Controller = require('egg').Controller;
const CategoryValid = require('../rule/category');
const {
  CREATE_FAIL,
  UPDATE_FAIL,
  QUERY_FAIL,
  SUCCESS,
  DELETE_FAIL,
  MISS_PARAM,
} = require('../constant');

class CatrgoryController extends Controller {
  /**
   * 新增一条分类信息
   */
  async addCategory() {
    // 参数
    const query = this.ctx.request.body;
    try {
      this.ctx.validate(CategoryValid.CREATE, query);
    } catch (error) {
      this.ctx.body = error;
      return;
    }
    // 接口请求参数
    const resp = await this.service.category.addCategory(query);
    // 如果新增成功
    if (resp) {
      this.ctx.body = { code: SUCCESS, message: '新增成功' };
      return;
    }
    // 新增失败
    this.ctx.body = { code: CREATE_FAIL, message: '新增失败' };
  }

  /**
   * 修改分类信息
   */
  async updateCategory() {
    // 请求参数
    const query = this.ctx.request.body;
    // 开始校验参数
    try {
      this.ctx.validate(CategoryValid.UPDATE, query);
    } catch (error) {
      this.ctx.body = error;
      return;
    }
    // 开始更新
    const resp = await this.ctx.service.category.updateCategory(query);
    // 如果更新成功
    if (resp) {
      this.ctx.body = { code: SUCCESS, message: '更新分类成功' };
      return;
    }
    this.ctx.body = { code: UPDATE_FAIL, message: '更新分类失败' };
  }

  /**
   * 获取所有的分类数据
   */
  async getAllCategory() {
    // 查出所有的分类数据
    const resp = await this.ctx.service.category.queryCategory();
    // 返回报文
    this.ctx.body = {
      code: SUCCESS,
      data: resp,
      msg: 'success',
    };
  }

  /**
   * 删除分类
   */
  async deleteCategory() {
    // 请求参数
    const query = this.ctx.request.body;
    // 如果当前没有传入分类id
    if (!query.id) {
      this.ctx.body = {
        code: MISS_PARAM,
        message: '缺少category id参数',
      };
      return;
    }
    // 查询出全局分类
    let idArr = [];
    try {
      // 查出所有的分类数据
      idArr = await this.ctx.service.category.queryCategoryById(query.id);
    } catch (error) {
      console.error(error);
      this.ctx.body = {
        code: QUERY_FAIL,
        message: '查询分类及子分类数据失败，请重试',
      };
      return;
    }
    // 删除当前分类以及其子分类
    try {
      await this.ctx.service.category.deleteCategory(idArr);
    } catch (error) {
      console.error(error);
      this.ctx.body = {
        code: DELETE_FAIL,
        message: '删除分类失败，请重试',
      };
      return;
    }
    this.ctx.body = {
      code: SUCCESS,
      message: '删除分类成功',
    };
  }
}

module.exports = CatrgoryController;
