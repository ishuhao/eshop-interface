'use strict';

const Controller = require('egg').Controller;
const ProductValid = require('../rule/product');
const {
  SUCCESS,
  CREATE_FAIL,
  QUERY_FAIL,
} = require('../constant');

class ProductController extends Controller {
  /**
   * 通过商品批次号查询商品信息
   */
  async queryProductBySkucode() {
    // 获取请求参数
    const { no } = this.ctx.query;
    // 将数据返回给前台
    this.ctx.body = this.ctx.service.product.findById(no);
  }

  /**
   * 创建一个商品
   */
  async createProduct() {
    // 请求参数
    const params = this.ctx.request.body;
    // 校验参数
    try {
      this.ctx.validate(ProductValid.CREATE, params);
    } catch (error) {
      this.ctx.body = error;
      return;
    }
    // 开始创建商品
    try {
      await this.ctx.service.product.createProduct(params);
    } catch (error) {
      console.log('创建商品失败：', error);
      this.ctx.body = {
        code: CREATE_FAIL,
        msg: '新增商品失败',
      };
      return;
    }
    // 返回报文
    this.ctx.body = {
      code: SUCCESS,
      msg: '新增商品成功',
    };
  }

  /**
   * 通过分页查询数据
   */
  async queryProductByPage() {
    // 请求参数
    const query = this.ctx.request.body || {};
    // 开始查询数据
    try {
      // 加载数据库中的数据
      const data = await this.ctx.service.product.queryProductByPage(query);
      // 将数据返回
      this.ctx.body = {
        code: SUCCESS,
        data,
        message: '查询成功',
      };
    } catch (error) {
      console.error(error);
      this.ctx.body = {
        code: QUERY_FAIL,
        message: '查询失败',
      };
    }
  }
}

module.exports = ProductController;
