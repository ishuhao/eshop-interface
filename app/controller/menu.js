'use strict';

const Controller = require('egg').Controller;
const MenuValid = require('../rule/menu');
const {
  SUCCESS,
  QUERY_FAIL,
  CREATE_FAIL,
  UPDATE_FAIL,
  MISS_PARAM,
  DELETE_FAIL,
  ERROR_PARAM,
} = require('../constant');

class MenuController extends Controller {
  /**
   * 查询所有的菜单信息
   */
  async queryMenuAll() {
    try {
      // 查询数据库的数据
      const menuList = await this.ctx.service.menu.queryAll();
      this.ctx.body = {
        code: SUCCESS,
        data: menuList,
        message: 'ok',
      };
    } catch (error) {
      console.error(error);
      // 查询错误，则返回查询错误code码
      this.ctx.body = {
        code: QUERY_FAIL,
        message: '查询失败',
      };
    }
  }

  /**
   * 新增一条菜单信息
   */
  async addMenu() {
    // 请求参数
    const query = this.ctx.request.body;
    try {
      // 开始校验参数
      this.ctx.validate(MenuValid.ADD, query);
    } catch (error) {
      this.ctx.body = error;
      return;
    }
    // 响应
    let resp = {
      code: SUCCESS,
      message: 'ok',
    };
    try {
      await this.ctx.service.menu.addMenu(query);
    } catch (error) {
      console.error(error);
      // 返回给接口的响应
      resp = {
        code: CREATE_FAIL,
        message: '新增菜单失败',
      };
    }
    this.ctx.body = resp;
  }

  /**
   * 编辑一条菜单信息
   */
  async editMenu() {
    // 请求参数
    const query = this.ctx.request.body;
    try {
      this.ctx.validate(MenuValid.UPDATE, query);
    } catch (error) {
      this.ctx.body = error;
      return;
    }
    // 返回报文
    let resp = {
      code: SUCCESS,
      message: '修改成功',
    };
    try {
      await this.ctx.service.menu.editMenu(query);
    } catch (error) {
      console.error(error);
      resp = {
        code: UPDATE_FAIL,
        message: '修改菜单失败',
      };
    }
    this.ctx.body = resp;
  }

  /**
   * 更新菜单的排序
   */
  async updateMenuSort() {
    // 请求参数
    const query = this.ctx.request.body;
    // 如果当前排序字段小于0
    if (query.sort < 0) {
      this.ctx.body = {
        code: ERROR_PARAM,
        message: 'sort字段值错误，请重试！',
      };
      return;
    }
    // 当前排序字段
    let menuItem = {};
    // 查询到当前的排序字段
    try {
      menuItem = await this.ctx.service.menu.queryOne(query);
    } catch (error) {
      console.error(error);
      // 查询失败
      this.ctx.body = {
        code: QUERY_FAIL,
        message: '查询上级菜单/下级菜单失败',
      };
      return;
    }
    try {
      await this.ctx.service.menu.updateMenuSort(query, menuItem);
    } catch (error) {
      console.error(error);
      this.ctx.body = {
        code: UPDATE_FAIL,
        message: '更新排序失败',
      };
      return;
    }
    this.ctx.body = {
      code: SUCCESS,
      message: '更新排序成功',
    };
  }

  /**
   * 删除指定的菜单项
   */
  async deleteMenu() {
    const { id } = this.ctx.query;
    // 如果没有传入参数
    if (!id) {
      this.ctx.body = {
        code: MISS_PARAM,
        message: '缺少参数id',
      };
      return;
    }
    try {
      await this.ctx.service.menu.deleteMenu(id);
    } catch (error) {
      console.error(error);
      // 返回接口错误码
      this.ctx.body = {
        code: DELETE_FAIL,
        message: '删除失败',
      };
      return;
    }
    // 删除成功
    this.ctx.body = {
      code: SUCCESS,
      message: 'ok',
    };
  }
}

module.exports = MenuController;
