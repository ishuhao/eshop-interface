'use strict';

module.exports = app => {
  const { INTEGER, STRING, DATE } = app.Sequelize;
  return app.model.define('cms', {
    id: {
      type: INTEGER(32),
      primaryKey: true,
      autoIncrement: true,
      comment: '主键',
    },
    cmsName: {
      type: STRING(100),
      allowNull: false,
      comment: '界面展示的cms名称',
    },
    name: {
      type: STRING(100),
      allowNull: false,
      comment: '接口请求中需要的cms名称',
    },
    type: {
      type: INTEGER(32),
      allowNull: false,
      comment: '当前类型',
    },
    status: {
      type: STRING(10),
      allowNull: false,
      comment: '当前配置的状态 0-已过期 1-已生效',
    },
    detail: {
      type: STRING(5000),
      allowNull: true,
      comment: '当前cms配置中的具体内容',
    },
    deleteFlag: {
      type: INTEGER(2),
      allowNull: true,
      comment: '当前是否是删除的',
    },
    createdMan: {
      type: INTEGER(32),
      allowNull: false,
      comment: '创建人',
    },
    createdAt: {
      type: DATE,
      defaultValue: app.Sequelize.NOW,
      comment: '创建时间',
    },
    updatedMan: {
      type: INTEGER(32),
      allowNull: false,
      comment: '更新人',
    },
    updatedAt: {
      type: DATE,
      defaultValue: app.Sequelize.NOW,
      comment: '更新时间',
    },
  });
};
