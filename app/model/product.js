'use strict';

module.exports = app => {
  const { INTEGER, STRING, DECIMAL, TEXT, DATE } = app.Sequelize;
  // 创建表接口
  return app.model.define('product', {
    id: {
      type: INTEGER(32),
      primaryKey: true,
      autoIncrement: true,
      comment: '商品数据库物理主键',
    },
    productNo: {
      type: STRING(32),
      // 唯一
      unique: true,
      // 非空
      allowNull: false,
      // 描述
      comment: '商品在仓库中的唯一编码',
      // 在数据库的表字段
      field: 'no',
    },
    productName: {
      type: STRING(500),
      allowNull: false,
      comment: '商品名称',
      filed: 'name',
    },
    productDesc: {
      type: STRING(1000),
      allowNull: true,
      comment: '商品描述，促销语',
      field: 'desc',
    },
    multiPrice: {
      type: DECIMAL(10, 2),
      allowNull: false,
      comment: '商品多价，商品进价（含税）',
      field: 'multi_price',
    },
    price: {
      type: DECIMAL(10, 2),
      allowNull: false,
      comment: '商品销售价格',
      field: 'price',
    },
    memberPrice: {
      type: DECIMAL(10, 2),
      allowNull: true,
      comment: '会员价',
      field: 'member_price',
    },
    min: {
      type: INTEGER(10),
      allowNull: true,
      comment: '最低购买量',
      defaultValue: 1,
      field: 'min',
    },
    max: {
      type: INTEGER(10),
      allowNull: true,
      comment: '最大购买量',
      defaultValue: 0,
      field: 'max',
    },
    picList: {
      type: STRING(1000),
      allowNull: false,
      comment: '商品图片集合，已经转换成json字符串了，最大只能上传5张',
      field: 'pic_list',
    },
    mainPic: {
      type: STRING(100),
      allowNull: false,
      comment: '商品主图',
    },
    subPic: {
      type: STRING(100),
      allowNull: false,
      comment: '商品副图',
    },
    productDetail: {
      type: TEXT,
      allowNull: false,
      comment: '商品详情，富文本集合',
      field: 'detail',
    },
    stockNum: {
      type: INTEGER(10),
      allowNull: false,
      defaultValue: 0,
      comment: '该商品的库存量',
      field: 'stock_num',
    },
    productState: {
      type: INTEGER(2),
      allowNull: false,
      defaultValue: 0,
      comment: '商品状态：0-未上架 1-已上架 2-已下架',
      field: 'state',
    },
    categoryId: {
      type: STRING(20),
      allowNull: false,
      comment: '商品所处分类的id',
      field: 'category_id',
    },
    categoryName: {
      type: STRING(500),
      allowNull: false,
      comment: '商品所处分类的名称',
      field: 'category_name',
    },
    productService: {
      type: STRING(1000),
      allowNull: true,
      comment: '商品提供的服务项',
      field: 'service',
    },
    productSku: {
      type: STRING(10000),
      allowNull: true,
      comment: '商品的sku属性',
      field: 'sku',
    },
    productTag: {
      type: STRING(100),
      allowNull: true,
      comment: '商品的标签',
      field: 'tag',
    },
    report: {
      type: STRING(1000),
      allowNull: true,
      comment: '质检报告，只能展示图片',
      field: 'report',
    },
    createdAt: {
      type: DATE,
      defaultValue: app.Sequelize.NOW,
      comment: '商品创建时间',
      field: 'created_at',
      allowNull: true,
    },
    updatedAt: {
      type: DATE,
      defaultValue: app.Sequelize.NOW,
      field: 'updated_at',
      comment: '商品更新时间',
      allowNull: true,
    },
    createdMan: {
      type: STRING(20),
      field: 'created_man',
      comment: '商品创建人',
      allowNull: true,
    },
    updatedMan: {
      type: STRING(20),
      field: 'updated_man',
      comment: '商品修改人',
      allowNull: true,
    },
  });
};
