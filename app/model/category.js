'use strict';

module.exports = app => {
  const { INTEGER, STRING } = app.Sequelize;
  return app.model.define('category', {
    id: {
      type: INTEGER(32),
      primaryKey: true,
      autoIncrement: true,
      allowNull: false,
      comment: '分类的主键id',
    },
    name: {
      type: STRING(500),
      allowNull: false,
      comment: '分类名称',
    },
    adv: {
      type: STRING(1000),
      allowNull: true,
      comment: '广告语',
    },
    desc: {
      type: STRING(1000),
      allowNull: true,
      comment: '分类描述',
    },
    advImg: {
      type: STRING(100),
      allowNull: true,
      comment: '商品主图',
    },
    link: {
      type: STRING(250),
      allowNull: true,
      comment: '跳转的链接',
    },
    sort: {
      type: INTEGER(5),
      allowNull: false,
      comment: '排序字段',
      defaultValue: 0,
    },
    parentId: {
      type: INTEGER(32),
      allowNull: true,
      defaultValue: 0,
      comment: '父分类id，如果当前没有父分类id，则当前是一级节点',
    },
  });
};
