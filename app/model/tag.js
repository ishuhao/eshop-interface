'use strict';

module.exports = app => {
  const { STRING, INTEGER } = app.Sequelize;
  return app.model.define('tag', {
    id: {
      type: INTEGER(32),
      primaryKey: true,
      autoIncrement: true,
      comment: '主键',
    },
    name: {
      type: STRING(100),
      defaultValue: '',
      allowNull: false,
      comment: '标签名称',
    },
  });
};
