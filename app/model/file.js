'use strict';

module.exports = app => {
  const { INTEGER, STRING } = app.Sequelize;
  return app.model.define('file', {
    id: {
      type: INTEGER(32),
      primaryKey: true,
      autoIncrement: true,
      comment: '文件的物理主键',
    },
    name: {
      type: STRING(500),
      comment: '处理后的文件名（唯一的）',
    },
    originName: {
      type: STRING(500),
      comment: '源文件名',
    },
    ext: {
      type: STRING(50),
      comment: '文件扩展名',
      defaultValue: 'folder',
    },
    size: {
      type: INTEGER(100),
      comment: '文件大小',
      defaultValue: 0,
    },
    path: {
      type: STRING(100),
      comment: '文件实际存放的路径',
      defaultValue: '',
    },
  });
};
