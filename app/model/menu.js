'use strict';

module.exports = app => {
  const { INTEGER, STRING } = app.Sequelize;
  return app.model.define('menu', {
    id: {
      type: INTEGER(32),
      primaryKey: true,
      autoIncrement: true,
      allowNull: false,
      comment: '菜单的主键id',
    },
    code: {
      type: STRING(300),
      allowNull: false,
      comment: '菜单编码',
    },
    title: {
      type: STRING(500),
      allowNull: false,
      comment: '菜单名称',
    },
    icon: {
      type: STRING(100),
      allowNull: true,
      comment: '菜单的iconfont图标名',
    },
    link: {
      type: STRING(500),
      allowNull: true,
      comment: '菜单的点击链接',
    },
    desc: {
      type: STRING(1000),
      allowNull: true,
      comment: '菜单的描述信息',
    },
    parentId: {
      type: INTEGER(32),
      allowNull: true,
      comment: '当前菜单的父级菜单',
    },
    sort: {
      type: INTEGER(10),
      allowNull: true,
      defaultValue: 0,
      comment: '排序字段',
    },
  });
};
