'use strict';

const Service = require('egg').Service;

class CategoryService extends Service {
  async addCategory({ name, desc, adv, advImg, link, parentId }) {
    // 结果
    let resp = null;
    try {
      // 调用创建接口
      resp = await this.ctx.model.Category.create({
        name,
        desc,
        adv,
        advImg,
        link,
        parentId,
      });
    } catch (error) {
      console.error(error);
    }
    return resp;
  }

  async updateCategory({ id, name, desc, adv, advImg, link, parentId }) {
    let resp = null;
    try {
      resp = await this.ctx.model.Category.update({
        name,
        desc,
        adv,
        advImg,
        link,
        parentId,
      }, {
        where: { id },
      });
    } catch (error) {
      console.error(error);
    }
    return resp;
  }

  async queryCategory() {
    const categorys = await this.ctx.model.Category.findAll() || [];
    // 分类列表
    const categoryList = [];
    // 将参数处理成嵌套格式
    categorys.forEach(category => {
      // 将数据转换成json
      const item = category.toJSON();
      // 取出父分类
      const node = this.getParentCategory(item.parentId, categoryList);
      // 如果当前分类没有父分类，则设为一级分类
      if (!node) {
        categoryList.push(item);
      } else {
        // 如果当前父分类没有子分类，则新建子分类数组
        if (!node.children) {
          node.children = [];
        }
        node.children.push(item);
      }
    });
    return categoryList;
  }

  /**
   * 查询指定分类id下的分类以及所有子分类
   * @param { Number } categoryId 分类id
   */
  async queryCategoryById(categoryId) {
    // 查询所有的分类id
    const categorys = await this.ctx.model.Category.findAll({ raw: true }) || [];
    const ids = this.getCategorId(categoryId, categorys);
    return [ categoryId, ...ids ];
  }

  /**
   * 在传入的分类列表中筛选出父id是当前需要筛选的分类
   * @param { Number } parentId 父id
   * @param { Array } list 当前分类集合
   */
  getCategorId(parentId, list = []) {
    // 分类id集合
    let ids = [];
    // 取出所有的id集合
    list.forEach(item => {
      if (parentId === item.parentId) {
        ids = [ item.id, ...this.getCategorId(item.id, list) ];
      }
    });
    return ids;
  }

  /**
   * 获取父节点
   * @param { Number } id   父节点
   * @param { Array }  list 需要处理的分类列表
   */
  getParentCategory(id, list = []) {
    // 当前节点
    let currentNode = list.find(item => item.id === id);
    // 如果当前获取到父节点
    if (currentNode) {
      return currentNode;
    }
    // 遍历当前节点，取出当前节点中的子节点
    list.forEach(item => {
      const node = this.getParentCategory(id, item.children);
      // 如果获取到符合条件的节点
      if (node) {
        currentNode = node;
      }
    });
    return currentNode;
  }

  /**
   * 删除分类及其下的子分类
   * @param { Array } idArr 分类集合
   */
  async deleteCategory(idArr = []) {
    const { Op } = this.app.Sequelize;
    // 开始删除数据
    await this.ctx.model.Category.destroy({
      where: {
        id: {
          [ Op.in ]: idArr,
        },
      },
    });
  }
}

module.exports = CategoryService;
