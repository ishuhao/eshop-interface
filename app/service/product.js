'use strict';

const Service = require('egg').Service;

class ProductService extends Service {
  async findById(productNo) {
    const resp = await this.ctx.model.Product.findOne({
      where: {
        productNo,
      },
    });
    return resp;
  }

  /**
   * 分页查询数据
   * @param { Object } query 请求参数
   */
  async queryProductByPage({ pageSize, page }) {
    // 每页展示数量
    const limit = pageSize || 10;
    // 偏移量
    const offset = ((page || 1) - 1) * limit;
    // 开始查询数据
    const data = await this.ctx.model.Product.findAndCountAll({
      limit, offset,
    });
    return data;
  }

  async createProduct(query) {
    const resp = await this.ctx.model.Product.create({
      productNo: query.no,
      productName: query.name,
      productDesc: query.desc,
      multiPrice: query.multiPrice,
      price: query.price,
      memberPrice: query.memberPrice,
      min: query.min,
      max: query.max,
      picList: JSON.stringify(query.picList || []),
      productDetail: query.detail,
      stockNum: query.stockNum,
      productState: query.state,
      categoryId: query.categoryId,
      productService: JSON.stringify(query.service || []),
      productSku: JSON.stringify(query.sku || []),
      productTag: JSON.stringify(query.tag || []),
      report: query.report,
    });
    return resp;
  }
}

module.exports = ProductService;
