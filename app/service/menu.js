'use strict';

const Service = require('egg').Service;

class MenuService extends Service {
  async queryAll() {
    const list = await this.app.model.Menu.findAll();
    // 菜单集合
    const menuList = [];
    // 遍历数据，先取出父节点数据
    list.forEach(item => {
      // 先将数据转换成json格式
      const menu = item.toJSON();
      // 如果当前没有父节点
      if (+menu.parentId === 0) {
        menuList.push(menu);
      }
    });
    // 将第一级菜单排序
    menuList.sort((before, next) => before.sort - next.sort);
    // 再次根据父菜单取出响应的子菜单
    menuList.forEach(item => {
      // 如果初始化子菜单
      item.children = [];
      list.forEach(child => {
        // 先将数据转换成json格式
        const menu = child.toJSON();
        // 如果当前没有父节点
        if (+menu.parentId === +item.id) {
          item.children.push(menu);
        }
      });
      // 将孩子节点排序
      item.children.sort((before, next) => before.sort - next.sort);
    });
    console.log('============== 已经查询出全局数据 ========>');
    return menuList;
  }

  /**
   * 查询出符合条件的数据
   * @param { Object } query 查询参数
   */
  async queryOne(query) {
    // 查询指定的数据
    const menuItem = await this.app.model.Menu.findOne({
      row: true,
      where: {
        parentId: query.parentId,
        sort: query.sort,
      },
    });
    return menuItem;
  }

  async addMenu(query) {
    await this.app.model.Menu.create({ ...query });
  }

  async editMenu(query) {
    await this.app.model.Menu.update({
      code: query.code,
      title: query.title,
      icon: query.icon,
      link: query.link,
      desc: query.desc,
      parentId: query.parentId,
      sort: query.sort,
    }, {
      where: { id: query.id },
    });
  }

  /**
   * 更新菜单的排序
   * @param { Object } query 请求参数
   * @param { Object } menu  当前占据此排序的菜单
   */
  async updateMenuSort(query, menu) {
    // 如果当前是向上走一位
    if (query.type === 'up') {
      menu.sort += 1;
    } else {
      menu.sort -= 1;
    }
    // 数据库对象
    const transaction = await this.ctx.model.transaction();
    try {
      // 先更新当前菜单
      await this.app.model.Menu.update({ sort: menu.sort }, { where: { id: menu.id }, transaction });
      // 再更新需要替换的菜单
      await this.app.model.Menu.update({ sort: query.sort }, { where: { id: query.id }, transaction });
      await transaction.commit();
    } catch (error) {
      await transaction.rollback();
      console.error(error);
    }
  }

  async deleteMenu(id) {
    const { Op } = this.app.Sequelize;
    // 将字符串转换成json
    const idArr = JSON.parse(id || '[]');
    return await this.app.model.Menu.destroy({
      where: {
        id: { [ Op.in ]: idArr },
      },
    });
  }
}

module.exports = MenuService;
