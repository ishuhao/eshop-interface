'use strict';

module.exports = {
  ADD: {
    code: {
      type: 'string',
      allowEmpty: false,
    },
    title: {
      type: 'string',
      allowEmpty: false,
    },
  },
  UPDATE: {
    id: {
      type: 'int',
      allowEmpty: false,
    },
    code: {
      type: 'string',
      allowEmpty: false,
    },
    title: {
      type: 'string',
      allowEmpty: false,
    },
  },
};
