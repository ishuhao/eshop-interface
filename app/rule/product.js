'use strict';

module.exports = {
  CREATE: {
    no: {
      type: 'string',
      allowEmpty: false,
    },
    name: {
      type: 'string',
      allowEmpty: false,
    },
    desc: {
      type: 'string',
      allowEmpty: true,
    },
    multiPrice: {
      type: 'number',
      allowEmpty: false,
    },
    price: {
      type: 'number',
      allowEmpty: false,
    },
    memberPrice: {
      type: 'number',
      allowEmpty: false,
    },
    min: {
      type: 'int',
      allowEmpty: true,
    },
    max: {
      type: 'int',
      allowEmpty: true,
    },
    picList: {
      type: 'array',
      allowEmpty: false,
    },
    detail: {
      type: 'string',
      allowEmpty: true,
    },
    stockNum: {
      type: 'int',
      allowEmpty: false,
    },
    state: {
      type: 'int',
      allowEmpty: false,
    },
    categoryId: {
      type: 'string',
      allowEmpty: false,
    },
    service: {
      type: 'array',
      allowEmpty: false,
    },
    sku: {
      type: 'array',
      allowEmpty: false,
    },
    tag: {
      type: 'array',
      allowEmpty: false,
    },
    report: {
      type: 'string',
      allowEmpty: true,
    },
  },
};
