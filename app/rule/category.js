'use strict';

module.exports = {
  UPDATE: {
    id: {
      type: 'int',
      allowEmpty: false,
    },
    name: {
      type: 'string',
      allowEmpty: false,
    },
  },
  CREATE: {
    name: {
      type: 'string',
      allowEmpty: false,
    },
  },
};
