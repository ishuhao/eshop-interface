'use strict';

/**
 * @param {Egg.Application} app - egg application
 */
module.exports = app => {
  require('./product')(app);
  require('./category')(app);
  require('./menu')(app);
  require('./file')(app);
};
