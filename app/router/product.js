'use strict';

module.exports = app => {
  const { router, controller } = app;
  router.get('/eshop/product/queryProductBySkucode', controller.product.queryProductBySkucode);
  router.post('/ecare/product/create', controller.product.createProduct);
  router.post('/ecare/product/list', controller.product.queryProductByPage);
};
