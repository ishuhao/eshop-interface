'use strict';

module.exports = app => {
  const { router, controller } = app;
  router.post('/ecare/menu/add', controller.menu.addMenu);
  router.post('/ecare/menu/update', controller.menu.editMenu);
  router.get('/ecare/menu/list', controller.menu.queryMenuAll);
  router.get('/ecare/menu/delete', controller.menu.deleteMenu);
  router.post('/ecare/menu/update/sort', controller.menu.updateMenuSort);
};
