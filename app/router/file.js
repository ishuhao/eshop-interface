'use strict';

module.exports = app => {
  const { router, controller } = app;

  // 查询所有文件
  router.get('/ecare/file/list', controller.file.listFile);
  // 上传文件
  router.post('/ecare/file/upload', controller.file.uploadFile);
  // 新建文件夹
  router.post('/ecare/file/folder/add', controller.file.addDirectory);
  // 删除文件夹
  router.get('/ecare/file/folder/delete', controller.file.deleteDirectory);
  // 修改文件夹名称
  router.post('/ecare/file/folder/edit', controller.file.addDirectory);
  // 下载文件
  router.get('/file/download/:name', controller.file.downloadFile);
};
