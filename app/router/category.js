'use strict';

module.exports = app => {
  const { router, controller } = app;
  // 新增分类信息
  router.post('/ecare/category/add', controller.category.addCategory);
  // 修改分类信息
  router.post('/ecare/category/update', controller.category.updateCategory);
  // 获取所有的分类信息
  router.get('/eshop/category/query', controller.category.getAllCategory);
  // 删除分类信息
  router.post('/ecare/category/delete', controller.category.deleteCategory);
};
